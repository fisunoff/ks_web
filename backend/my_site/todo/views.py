from django.shortcuts import render, get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import viewsets
from .models import *
from .serializers import *


class RecordViewSet(viewsets.ModelViewSet):
    serializer_class = RecordSerializer

    queryset = Record.objects.all()


class AuthorViewSet(viewsets.ModelViewSet):
    serializer_class = AuthorSerializer

    queryset = Author.objects.all()


class TagsViewSet(viewsets.ModelViewSet):
    serializer_class = TagsSerializer

    queryset = Tags.objects.all()