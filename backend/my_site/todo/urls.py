from rest_framework.routers import DefaultRouter
from .views import *

#urlpatterns = [
#    path('information/', RecordViewSet.as_view({"get": "list"})),
#    path('information/<int:pk>', RecordViewSet.as_view({"get": "retrieve"}))
#]
router = DefaultRouter()

router.register(r'record', RecordViewSet, basename="record")
router.register(r'author', AuthorViewSet, basename="author")
router.register(r'tags', TagsViewSet, basename="tags")
urlpatterns = router.urls
