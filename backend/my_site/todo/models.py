from django.db import models


# Create your models here.
from django.utils import timezone


class Author(models.Model):
    id = models.AutoField(verbose_name="id", primary_key=True)
    name = models.CharField(verbose_name="Имя", max_length=200)
    department = models.CharField(verbose_name="Подразделение", max_length=200)
    is_active = models.BooleanField(verbose_name="Действующий автор", default=True)
    time_create = models.DateTimeField(verbose_name="Дата создания учетной записи", default=timezone.now)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Автор"
        verbose_name_plural = "Авторы"


class Record(models.Model):
    id = models.AutoField(verbose_name="id", primary_key=True)
    title = models.CharField("Название", max_length=200)
    text = models.TextField("Текст записи")
    author = models.ForeignKey("Author", on_delete=models.SET_NULL, null=True)
    tag = models.ForeignKey("Tags", on_delete=models.SET_NULL, null=True)
    status = models.CharField("Статус", max_length=20, default="Новый")

    def __str__(self):
        return self.title

    def get_tag(self):
        return self.tag

    class Meta:
        verbose_name = "Запись"
        verbose_name_plural = "Записи"


class Tags(models.Model):
    id = models.AutoField(verbose_name="id", primary_key=True)
    tag_title = models.CharField("Название тега", max_length=200)
    priority = models.CharField("Приоритет", max_length=20, default="minor")
    active_tag = models.BooleanField("Используемый тег", default=True)

    def __str__(self):
        return self.tag_title

    class Meta:
        verbose_name = "Тег"
        verbose_name_plural = "Теги"
